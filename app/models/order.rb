class Order < ActiveRecord::Base
  belongs_to :order_status
  has_many :order_items
  belongs_to :user
  before_create :set_order_status
  before_save :update_subtotal

  def subtotal
    order_items.collect { |oi| oi.valid? ? (oi.quantity * oi.unit_price) : 0 }.sum
  end
private
  def set_order_status
    self.order_status_id = 1
  end

  def update_subtotal
    self[:subtotal] = subtotal
  end
end

  def final
    self.order_status_id = 2
    self.user_id = current_user.id
    @userid = current_user.id
    @order.user_id = current_user.id
    #@order_items = destroy
    order.save
    
  end
  
  
#    validates :name, :email, presence: true
#    serialize :notification_params, Hash
#     def paypal_url(return_path)
#     values = {
#      business: "johnny.p.norton-facilitator@gmail.com",
#      cmd: "_xclick",
#      upload: 1,
#      return: "#{Rails.application.secrets.app_host}#{return_path}",
#     invoice: id,
#      amount: order.price,
#     item_album: album.name,
#      album_number: album.id,
#      quantity: '1',
#      notify_url: "#{Rails.application.secrets.app_host}/hook"
#    }
#    "#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?" + values.to_query
#  end
  











