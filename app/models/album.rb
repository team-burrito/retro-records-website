class Album < ActiveRecord::Base
  
  belongs_to :genre
  belongs_to :artist
  has_many :order_items
  
  # validates :name, :description, :price, :presence => true
  #validates :price, numericality => true
  
  has_attached_file :image, styles: { large: "900x900>", medium: "450x450>", thumb: "200x200>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  
  
  
end
 