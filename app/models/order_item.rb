class OrderItem < ActiveRecord::Base
  belongs_to :album
  belongs_to :order
  belongs_to :user

  validates :quantity, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validate :order_present

  before_save :finalize

  def unit_price
    if persisted?
      self[:unit_price]
    else
      album.price
    end
  end
  
  def total_price
    unit_price * quantity
  end


  def order_present
    if order.nil?
      errors.add(:order, "is not a valid order.")
    end
  end

  
  
  def finalize
    self[:unit_price] = unit_price
    self[:total_price] = quantity * self[:unit_price]
    self[:album_id] = album_id
  end
end