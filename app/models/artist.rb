class Artist < ActiveRecord::Base
  
  has_many :albums
  
  has_attached_file :image, styles: { large: "1920x1920>", medium: "450x450>", thumb: "200x200>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  
end
