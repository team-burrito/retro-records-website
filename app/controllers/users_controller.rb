class UsersController < ApplicationController
  before_filter :verify_is_admin
  
  
  
    def verify_is_admin
      (current_user.nil?) ? redirect_to(new_user_session_path) : (redirect_to(new_user_session_path) unless current_user.admin?)
    end
end
