class StoreController < ApplicationController
  def index
    @albums = Album.order(:id)
    @genres = Genre.order(:name)
    @artists = Artist.order(:name)
    
  end #automatically load: app/views/store/index.html.erb

  def show
    
    @album = Album.find(params[:id])
    @order_item = current_order.order_items.new
  end #automatically load: app/views/store/show.html.erb
  
  def myorders
    @orders = current_user.orders
    @order_items = OrderItem.all
    
  end
  
  def genre
    @genre = Genre.find(params[:id])
    @genres = Genre.order(:name)
  end #automatically load: app/views/store/genre.html.erb
  
  def artist
    @artist = Artist.find(params[:id])
  end 
  
  def genrelayout
    @genres = Genre.order(:name)
  end 
  
  def albumlayout
    @albums = Album.order(:name)
  end 
  
  def artistlayout
    @artists = Artist.order(:name)
  end 
  
  # SEARCH
   def search
     @search_term = params[:q]
     st = "%#{params[:q]}%"
     @albums = Album.where("Name ILIKE ? or Description ILIKE ?", st, st)
    
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @albums }
    end
  end

end
