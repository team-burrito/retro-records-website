class AdminController < ApplicationController
#class AdminController < ActionController::Base
 before_filter :verify_is_admin


  def admin
  end

  private
  
  def verify_is_admin
    (current_user.nil?) ? redirect_to(new_user_session_path) : (redirect_to(new_user_session_path) unless current_user.admin?)
  end
end