class AddBillingShippingPhoneToUsers < ActiveRecord::Migration
  def change
    add_column :users, :billing_address_line1, :string
    add_column :users, :billing_address_line2, :string
    add_column :users, :billing_address_city, :string
    add_column :users, :shipping_address_line1, :string
    add_column :users, :shipping_address_line2, :string
    add_column :users, :shipping_address_city, :string
    add_column :users, :delivery_details, :text
    add_column :users, :phone_number, :decimal
  end
end
