class UpdatePhoneTypeVarchar < ActiveRecord::Migration
  def change
    change_column :users, :phone_number, :varchar
  end
end
