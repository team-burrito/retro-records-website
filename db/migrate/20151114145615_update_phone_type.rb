class UpdatePhoneType < ActiveRecord::Migration
  def change
    change_column :users, :phone_number, :integer
  end
end
