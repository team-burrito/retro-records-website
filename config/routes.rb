Rails.application.routes.draw do
 
 post '/final' => 'orders#final'
# post "/orders/:id" => "orders#show"
# post "/hook" => "orders#hook"

 # resources :products, only: [:index]
  resource :cart, only: [:show]
  resources :order_items, only: [:create, :update, :destroy]
 
  get 'order_items/create'

  get 'order_items/update'

  get 'order_items/destroy'

  get 'carts/show'
  
  get '/myorders' => 'store#myorders'
  
  post '/search' => 'store#search'


  resources :orders
  devise_for :users, :controllers => { registrations: 'user/registrations' }
  # store home page
  root :to => 'store#index', :via => :get
  
  # single-item layouts
  match '/store/album/:id' => 'store#show', :as => :store_album, :via => :get
  match '/store/genre/:id' => 'store#genre', :as => :store_genre, :via => :get
  match '/store/artist/:id' => 'store#artist', :as => :store_artist, :via => :get
  match 'users/:id' => 'users#destroy', :via => :delete, :as => :admin_destroy_user
  # multi-item layouts
  get '/store/genres', to: 'store#genrelayout'
  get '/store/albums', to: 'store#albumlayout'
  get '/store/artists', to: 'store#artistlayout'
  
  # admin links
  
  resources :orders, :path => "admin/orders"
  resources :artists, :path => "admin/artists"
  resources :genres, :path => "admin/genres"
  resources :albums, :path => "admin/albums"
  resources :users, :path => "admin/users"
  get '/admin', to: 'admin#admin'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'application#hello'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
